(define-module (nominatim packages nominatim)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages php)
  #:use-module (gnu packages python)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (nominatim packages pyosmium))

(define-public nominatim
  (package
   ;;; FIXME: some files reference the source
   ;;; FIXME: documentation does not build
   (name "nominatim")
   (version "3.3.0")
   (source (origin
	    (method url-fetch)
	    (uri (string-append "https://www.nominatim.org/release/Nominatim-"
				version ".tar.bz2"))
	    (sha256
	     (base32
	      "1y8alvq6d423qdfq9230kzzhifphzz590dd144m7jvdfk8jfkwi4"))))
   (build-system cmake-build-system)
   (arguments '(#:tests? #f ; FIXME: Make tests work.
		#:phases
		(modify-phases %standard-phases
			       (add-after 'unpack 'install-source
					  (lambda* (#:key outputs #:allow-other-keys)
						   (mkdir-p (assoc-ref outputs "source"))
						   (copy-recursively "." (assoc-ref outputs "source"))
						   #t))
			       (add-after 'install 'install-module
					  (lambda* (#:key outputs #:allow-other-keys)
						   (mkdir-p (assoc-ref outputs "module"))
						   (copy-file "module/nominatim.so" (string-append (assoc-ref outputs "module") "/nominatim.so"))
						   #t))
			       (add-after 'install 'install-nominatim
					  (lambda* (#:key outputs #:allow-other-keys)
						   (mkdir-p (assoc-ref outputs "nominatim"))
						   (copy-file "nominatim/nominatim" (string-append (assoc-ref outputs "nominatim") "/nominatim"))
						   #t))
			       (add-after 'install 'install-utils
					  (lambda* (#:key outputs #:allow-other-keys)
						   (mkdir-p (assoc-ref outputs "utils"))
						   (copy-recursively "utils" (assoc-ref outputs "utils"))
						   #t))
			       (add-after 'install 'install-website
					  (lambda* (#:key outputs #:allow-other-keys)
						   (mkdir-p (assoc-ref outputs "website"))
						   (copy-recursively "website" (assoc-ref outputs "website") #:follow-symlinks? #t)
						   #t))
			       (add-after 'install 'install-install-path
					  (lambda* (#:key outputs #:allow-other-keys)
						   (mkdir-p (assoc-ref outputs "install-path"))
						   (symlink (assoc-ref outputs "nominatim") (string-append (assoc-ref outputs "install-path") "/nominatim"))
						   #t)))))
   (inputs `(("boost" ,boost)
	     ("expat" ,expat)
	     ("libxml2" ,libxml2)
	     ("php" ,php)
	     ("postgresql" ,postgresql)
	     ("proj.4" ,proj.4)
	     ("pyosimum" ,pyosmium)
	     ("zlib" ,zlib)))
   (outputs '("out" "install-path" "module" "nominatim" "source" "utils" "website"))
   (native-inputs `(("python" ,python)))
   (synopsis #f)
   (description #f)
   (home-page "nominatim.org")
   (license license:gpl2)))
