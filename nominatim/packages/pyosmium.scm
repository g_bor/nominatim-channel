(define-module (nominatim packages pyosmium)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system python)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (nominatim packages libosmium))

(define-public pyosmium
  (package
   (name "pyosmium")
   (version "2.15.2")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/osmcode/pyosmium.git")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "0bnlk8d01kn7219rxnnzn9x0bzcxfi52r533c6xci6qijdxyfbd2"))))
   (build-system python-build-system)
   (inputs `(("boost" ,boost)
	     ("expat" ,expat)
	     ("libosmium" ,libosmium)
	     ("protozero" ,protozero)
	     ("zilb" ,zlib)))
   (native-inputs `(("cmake" ,cmake)
		    ("pybind11" ,pybind11)))
   (home-page "https://osmcode.org/pyosmium/")
   (synopsis #f)
   (description #f)
   (license license:bsd-2)))
