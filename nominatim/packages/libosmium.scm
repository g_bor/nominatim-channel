(define-module (nominatim packages libosmium)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages protobuf)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public libosmium
  (package
   (name "libosmium")
   (version "2.15.1")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/osmcode/libosmium.git")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32
	      "1v1m068lcjngxnwirpi0vqjhqnxn9gqvafjp3sy14vzmgl2sw2kr"))))
   (build-system cmake-build-system)
   (inputs `(("boost" ,boost)
	     ("expat" ,expat)
	     ("gdal" ,gdal)
	     ("geos" ,geos)
	     ("proj.4" ,proj.4)
	     ("protozero" ,protozero)
	     ("sparsehash" ,sparsehash)
	     ("zlib" ,zlib)))
   (home-page "https://osmcode.org/libosmium/")
   (synopsis #f)
   (description #f)
   (license license:boost1.0)))
